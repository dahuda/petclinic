package com.javaegitimleri.petclinic.dao;

import com.javaegitimleri.petclinic.model.*;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/10/13
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
@Repository("petClinicDao")
public class PetClinicDaoHibernateImpl implements PetClinicDao
{

    @Autowired
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Collection<Vet> getVets() {
        Session session = sessionFactory.openSession();
        try{
            Query query = session.createQuery("select distinct v from Vet v left join fetch v.specialties");
            return query.list();
        } finally {
            session.close();
        }
    }

    @Override
    public List findOwners(String lastName) {
        Session session = sessionFactory.openSession();
        try{
            Query query = session.createQuery("from Owner o where o.lastName = :lastName");
            query.setParameter("lastName", lastName);
            return query.list();
        } finally {
            session.close();
        }
    }

    @Override
    public Collection<Visit> findVisits(long petId) {
        return sessionFactory.getCurrentSession().createQuery("from Visit v where v.pet.id = ?").setParameter(0, petId).list();
    }

    @Override
    public Collection<Person> findAllPersons() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Owner loadOwner(long id) {
        return (Owner) sessionFactory.getCurrentSession().get(Owner.class, id);
    }

    @Override
    public Pet loadPet(long id) {
        return (Pet) sessionFactory.getCurrentSession().createQuery("from Pet p where p.id = ?").setParameter(0, id).uniqueResult();
    }

    @Override
    public Vet loadVet(long id) {

        /*
        Vet vet = (Vet) sessionFactory.getCurrentSession().get(Vet.class, id);
        Hibernate.initialize(vet);
        return vet;
        */

        return (Vet) sessionFactory.getCurrentSession()
                .createQuery("select new com.javaegitimleri.petclinic.model.Vet(id, firstName, lastName) from Vet where id = ?")
                .setParameter(0, id).uniqueResult();

    }

    @Override
    public void saveOwner(Owner owner) {
        sessionFactory.getCurrentSession().saveOrUpdate(owner);
    }

    @Override
    public void saveVet(Vet vet) {
        sessionFactory.getCurrentSession().save(vet);
    }

    @Override
    public void deleteOwner(long ownerId) {
        Owner owner = new Owner();
        owner.setId(ownerId);
        sessionFactory.getCurrentSession().delete(owner);
    }

}
