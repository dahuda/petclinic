package com.javaegitimleri.petclinic.dao;

import com.javaegitimleri.petclinic.model.*;

import java.util.Collection;
import java.util.List;


public interface PetClinicDao {
	
	Collection<Vet> getVets();

	List findOwners(String lastName);
	
	Collection<Visit> findVisits(long petId);
	
	Collection<Person> findAllPersons();

	Owner loadOwner(long id);

	Pet loadPet(long id);
	
	Vet loadVet(long id);

	void saveOwner(Owner owner);

	void saveVet(Vet vet);

	void deleteOwner(long ownerId);
}
