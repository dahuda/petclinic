package com.javaegitimleri.petclinic.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/12/13
 * Time: 1:44 PM
 * To change this template use File | Settings | File Templates.
 */
@Aspect
public class PetClinicTrackerAspect
{

    //@Around("execution(* com.javaegitimleri..*.*(..))")
    @Around("bean(*Service)")
    public Object trace(ProceedingJoinPoint pjp) throws Throwable
    {
        try {
            System.out.println("Before : "+pjp.getSignature());
            return pjp.proceed();
        }  finally {
            System.out.println("After : "+pjp.getSignature());
        }
    }

}
