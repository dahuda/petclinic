package com.javaegitimleri.petclinic.web;


import com.javaegitimleri.petclinic.model.Vet;
import com.javaegitimleri.petclinic.service.PetClinicService;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/11/13
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
@WebServlet(name = "PetClinicServlet")
public class PetClinicServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String action = request.getParameter("action");
        if ("getVets".equals(action)) {
            WebApplicationContext webAppContext  = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
            PetClinicService petClinicService =  webAppContext.getBean(PetClinicService.class);

            Collection<Vet> vets = petClinicService.getVets();

            response.getWriter().write(StringUtils.collectionToCommaDelimitedString(vets));

        }  else {
            response.getWriter().write("unknown action :" + action);
        }
    }

}
