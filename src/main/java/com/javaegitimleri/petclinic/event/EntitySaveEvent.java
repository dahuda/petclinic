package com.javaegitimleri.petclinic.event;

import com.javaegitimleri.petclinic.model.BaseEntity;
import org.springframework.context.ApplicationEvent;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/10/13
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class EntitySaveEvent extends ApplicationEvent
{

    private BaseEntity entity;

    public  EntitySaveEvent(Object source)
    {
        super(source);
    }

    public BaseEntity getEntity() {
        return entity;
    }

    public void setEntity(BaseEntity entity) {
        this.entity = entity;
    }
}
