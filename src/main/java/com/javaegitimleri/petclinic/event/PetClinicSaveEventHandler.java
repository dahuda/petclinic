package com.javaegitimleri.petclinic.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/10/13
 * Time: 10:04 AM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class PetClinicSaveEventHandler implements ApplicationListener<EntitySaveEvent>
{

    @Override
    public void onApplicationEvent(EntitySaveEvent event)
    {
        System.out.println("Entity save event handled oldu: "+event);
    }

}
