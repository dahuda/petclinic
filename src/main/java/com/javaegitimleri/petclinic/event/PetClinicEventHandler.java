package com.javaegitimleri.petclinic.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class PetClinicEventHandler implements ApplicationListener {

    @Override
    public void onApplicationEvent(ApplicationEvent event) {

        if (event instanceof ContextRefreshedEvent) {
            System.out.println("ContextRefreshedEvent");
        } else if (event instanceof ContextClosedEvent) {
            System.out.println("ContextClosedEvent");
        }
    }

}