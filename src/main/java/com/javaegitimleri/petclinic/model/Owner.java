package com.javaegitimleri.petclinic.model;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


@Entity
@PrimaryKeyJoinColumn
@Table(name="owners")
public class Owner extends Person {
	
	private static final long serialVersionUID = 1L;
	
	@Embedded
	private Address address = new Address(this);

	@OneToMany(fetch = FetchType.EAGER,mappedBy="owner")
	private Set<Pet> pets = new HashSet<Pet>();
	
	public Set<Pet> getPets() {
		return Collections.unmodifiableSet(pets);
	}
	
	public void addPet(Pet pet) {
		pet.setOwner(this);
		pets.add(pet);
	}

	public Address getAddress() {
		return address;
	}
}
