package com.javaegitimleri.petclinic.service;

import com.javaegitimleri.petclinic.dao.PetClinicDao;
import com.javaegitimleri.petclinic.event.EntitySaveEvent;
import com.javaegitimleri.petclinic.model.*;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Collection;

@Service("petClinicService")
@Transactional
public class PetClinicServiceImpl implements PetClinicService, InitializingBean, DisposableBean, ApplicationContextAware
{
	
	private PetClinicDao petClinicDao;

    private ApplicationContext appContext;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Autowired
	public PetClinicServiceImpl(@Qualifier("petClinicDao")PetClinicDao petClinicDao) {
        System.out.println("PetClinicService created");
		this.petClinicDao = petClinicDao;
	}

    public void setPetClinicDao(PetClinicDao petClinicDao)
    {
        this.petClinicDao = petClinicDao;
    }

	public Collection<Vet> getVets() {
		return petClinicDao.getVets();
	}

	public Collection<Owner> findOwners(String lastName) {
		return petClinicDao.findOwners(lastName);
	}

	public Collection<Visit> findVisits(long petId) {
		return petClinicDao.findVisits(petId);
	}

	public Collection<Person> findAllPersons() {
		return petClinicDao.findAllPersons();
	}

	public Owner loadOwner(long id) {
		return petClinicDao.loadOwner(id);
	}

	public Pet loadPet(long id) {
		return petClinicDao.loadPet(id);
	}

	public Vet loadVet(long id) {
		return petClinicDao.loadVet(id);
	}

	public long saveOwner(Owner owner) {
		petClinicDao.saveOwner(owner);
		return owner.getId();
	}

	public void saveVet(final Vet vet) {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                petClinicDao.saveVet(vet);
                EntitySaveEvent event = new EntitySaveEvent(this);
                event.setEntity(vet);
                appContext.publishEvent(event);
            }
        });


	}

	public void deleteOwner(long ownerId) {
		petClinicDao.deleteOwner(ownerId);
	}

    @Override
    public void destroy() throws Exception {
        System.out.println("PetClinicServiceImpl destroy invoked");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("PetClinicServiceImpl afterPropertiesSet invoked");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        appContext =  applicationContext;
    }

}
