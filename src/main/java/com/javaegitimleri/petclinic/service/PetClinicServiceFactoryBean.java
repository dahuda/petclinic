package com.javaegitimleri.petclinic.service;

import com.javaegitimleri.petclinic.dao.PetClinicDao;
import org.springframework.beans.factory.FactoryBean;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/9/13
 * Time: 2:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class PetClinicServiceFactoryBean implements FactoryBean<PetClinicService>
{
    private PetClinicDao petClinicDao;

    public void setPetClinicDao(PetClinicDao petClinicDao) {
        this.petClinicDao = petClinicDao;
    }

    @Override
    public PetClinicService getObject() throws Exception {
        PetClinicServiceImpl petClinicService = new PetClinicServiceImpl(petClinicDao);
        return petClinicService;
    }

    @Override
    public Class<?> getObjectType() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean isSingleton() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

}
