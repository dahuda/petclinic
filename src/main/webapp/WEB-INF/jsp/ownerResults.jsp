<%@ page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body> Owners List - <br/>
<c:forEach items="${owners}" var="o">
    ${o.id} - ${o.firstName} ${o.lastName} <br />
</c:forEach>
</body>
</html>