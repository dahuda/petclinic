package com.javaegitimleri.petclinic.tests;

import com.javaegitimleri.petclinic.service.PetClinicService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/12/13
 * Time: 10:21 AM
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/client.xml")
public class HttpInvokerTests
{

    @Autowired
    private PetClinicService petClinicService;

    @Test
    public void testHttpInvoker()
    {
        System.out.println("Vet returned : "+petClinicService.loadVet(1L));
    }

}
