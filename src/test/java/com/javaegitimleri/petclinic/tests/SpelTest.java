package com.javaegitimleri.petclinic.tests;

import com.javaegitimleri.petclinic.model.Vet;
import org.junit.Test;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/10/13
 * Time: 11:14 AM
 * To change this template use File | Settings | File Templates.
 */
public class SpelTest
{

    @Test
    public void spelTest()
    {
        Vet vet = new Vet();
        vet.setFirstName("Mahsun");

        System.out.println("before "+vet);

        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("firstName");

        StandardEvaluationContext context = new StandardEvaluationContext(vet);

        Expression expression2 = parser.parseExpression("lastName");
        expression2.setValue(context, "Kırmızıgül");

        String firstname = (String) expression.getValue(context);

        System.out.println(firstname);
        System.out.println("afet "+vet);

    }

}
