package com.javaegitimleri.petclinic.tests;

import com.javaegitimleri.petclinic.model.Owner;
import com.javaegitimleri.petclinic.model.Vet;
import com.javaegitimleri.petclinic.service.PetClinicService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/appcontext/beans-*.xml"})
public class SpringTests
{

    @Autowired
    private PetClinicService petClinicService;

    @Autowired
    private ApplicationContext applicationContext;

    @Before
    public void setUp()
    {}

    @Test
    public void testGetVets()
    {
        Collection<Vet> vets = petClinicService.getVets();
        for(Vet vet: vets) {
            System.out.println(vet);
        }
    }

    @Test
    @Transactional
    @Rollback(false)
    public void testSaveVet()
    {
        Vet vet = new Vet();
        vet.setFirstName("Hüsnü");
        vet.setLastName("Senlendirici");
        vet.save();
    }

    @Test
    public void testFindOwner()
    {
        String lastName = "Black";
        Collection<Owner>  owners = petClinicService.findOwners(lastName);
        System.out.println("List of owners : "+owners);
    }
}
