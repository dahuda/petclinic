package com.javaegitimleri.petclinic.tests;

import com.javaegitimleri.petclinic.model.Vet;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

/**
 * Created with IntelliJ IDEA.
 * User: kunal
 * Date: 5/11/13
 * Time: 5:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class RestTest
{

    @Test
    public void testLoadVet()
    {
        RestTemplate restTemplate = new RestTemplate();
        Vet vet = restTemplate.getForObject("http://localhost:8080/petclinic/mvc/vet/{vetId}", Vet.class, 3L);
        System.out.println(vet);
    }

}
